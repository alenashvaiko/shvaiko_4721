﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car.Library
{
    class AutoInfo : CarInfo
    {
        private string _Rule="";

        public string Rule
        {
            get
            {
                return _Rule;
            }
            set
            {
                _Rule = value;
            }
        }
    }
}
